GeneFlow App for Masking Repeats w/ MUMmer
==========================================

Version: 0.2

This GeneFlow app masks repeats in a reference sequence using Mummer.

Inputs
------

1. reference_sequence: Reference sequence FASTA file.

Parameters
----------

1. threads: CPU threads - Number of CPU threads to use for nucmer. Default: 2.
2. output: Output directory - The name of the output directory to place the fasta, fai, and dict files. Default: output.

